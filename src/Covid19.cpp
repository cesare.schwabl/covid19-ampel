/*
 ██████╗ ██████╗ ██╗   ██╗██╗██████╗  ██╗ █████╗        █████╗ ███╗   ███╗██████╗ ███████╗██╗     
██╔════╝██╔═══██╗██║   ██║██║██╔══██╗███║██╔══██╗      ██╔══██╗████╗ ████║██╔══██╗██╔════╝██║     
██║     ██║   ██║██║   ██║██║██║  ██║╚██║╚██████║█████╗███████║██╔████╔██║██████╔╝█████╗  ██║     
██║     ██║   ██║╚██╗ ██╔╝██║██║  ██║ ██║ ╚═══██║╚════╝██╔══██║██║╚██╔╝██║██╔═══╝ ██╔══╝  ██║     
╚██████╗╚██████╔╝ ╚████╔╝ ██║██████╔╝ ██║ █████╔╝      ██║  ██║██║ ╚═╝ ██║██║     ███████╗███████╗
 ╚═════╝ ╚═════╝   ╚═══╝  ╚═╝╚═════╝  ╚═╝ ╚════╝       ╚═╝  ╚═╝╚═╝     ╚═╝╚═╝     ╚══════╝╚══════╝
  
  modified 25 Sept 2020
  by Cesare Schwabl
*/

#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include "ArduinoJson.h"
#include "Adafruit_NeoPixel.h"

// ##################################
// ########  Pin Definition  ########
// ##################################
#define LED_PIN 23
#define NUM_PIXELS 4
#define BUTTON_PIN 2
#define BUTTON_PIN_BITMASK 0x200000000 // 2^33 in hex

// ##################################
// ########  User  Settings  ########
// ##################################
const char* ssid       = "happylab"; 
const char* password   = "happylab";
const char* Stadt      = "Wien";      // z.B. Wien

// ##################################
// #########   Variablen     ########
// ##################################
int buttonState;                      // the current reading from the input pin
int lastButtonState = HIGH;           // the previous reading from the input pin
unsigned long lastDebounceTime = 0;   // the last time the output pin was toggled
unsigned long debounceDelay = 500;    // the debounce time; increase if the output flickers
auto ampel_date = "";
int timeout = 10000;
RTC_DATA_ATTR int bootCount = 0;

// ##################################
// ######### Funktionsdef.   ########
// ##################################
void print_wakeup_reason();
void setAmpelFarbe(int farbe);
int getWarnstufe(const char* Stadt);
void initAmpel();
void clearStrip();
void setAmpelFarbe(int farbe);
uint32_t dimColor(uint32_t color, uint8_t width);
void knightRider(uint16_t cycles, uint16_t speed, uint8_t width, uint32_t color);
void measure_battery();
void prepareAndGoToDeepSleep();

// ##################################
// #####   Init Json + Pixel     ####
// ##################################
Adafruit_NeoPixel strip(NUM_PIXELS, LED_PIN, NEO_GRB + NEO_KHZ800);  
const size_t capacity = JSON_ARRAY_SIZE(1) + JSON_ARRAY_SIZE(65) + JSON_OBJECT_SIZE(2) + 65*JSON_OBJECT_SIZE(4) + 1880;
DynamicJsonDocument doc(capacity);

void setup() {
  Serial.begin(115200);
  delay(100);

  ++bootCount; //Increment boot number and print it every reboot
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_2,0); //1 = High, 0 = Low
  
  strip.begin();
  clearStrip(); // Initialize all pixels to 'off'

  delay(10);
  Serial.println();
  Serial.println(" ██████╗ ██████╗ ██╗   ██╗██╗██████╗  ██╗ █████╗        █████╗ ███╗   ███╗██████╗ ███████╗██╗     ");   
  Serial.println("██╔════╝██╔═══██╗██║   ██║██║██╔══██╗███║██╔══██╗      ██╔══██╗████╗ ████║██╔══██╗██╔════╝██║     ");    
  Serial.println("██║     ██║   ██║██║   ██║██║██║  ██║╚██║╚██████║█████╗███████║██╔████╔██║██████╔╝█████╗  ██║     ");   
  Serial.println("██║     ██║   ██║╚██╗ ██╔╝██║██║  ██║ ██║ ╚═══██║╚════╝██╔══██║██║╚██╔╝██║██╔═══╝ ██╔══╝  ██║     ");  
  Serial.println("╚██████╗╚██████╔╝ ╚████╔╝ ██║██████╔╝ ██║ █████╔╝      ██║  ██║██║ ╚═╝ ██║██║     ███████╗███████╗");
  Serial.println(" ╚═════╝ ╚═════╝   ╚═══╝  ╚═╝╚═════╝  ╚═╝ ╚════╝       ╚═╝  ╚═╝╚═╝     ╚═╝╚═╝     ╚══════╝╚══════╝");
  Serial.println();
  Serial.println("Boot number: " + String(bootCount));
  Serial.println();
  print_wakeup_reason();
  Serial.println();
  Serial.printf("Verbinde zu Netzwerk %s\n",ssid);
  WiFi.begin(ssid, password);
  Serial.print(".");
  while (WiFi.status() != WL_CONNECTED && millis() < 10000)
  {
      delay(500);
      Serial.print(".");
  }
  Serial.print(" OK\n");
  Serial.print("IP Addresse: ");
  Serial.println(WiFi.localIP());

  knightRider(6, 128, 2, 0xFFFFFF);
  clearStrip();
  clearStrip();

  measure_battery();
  Serial.println();

  int warnstufe = getWarnstufe(Stadt);  
  setAmpelFarbe(warnstufe);

  void prepareAndGoToDeepSleep();
}

void loop() {
  
}

int getWarnstufe(const char* Stadt) {
  HTTPClient http;  //Object of class HTTPClient
  http.begin("https://corona-ampel.gv.at/sites/corona-ampel.gv.at/files/assets/Warnstufen_Corona_Ampel_aktuell.json");
  int httpCode = http.GET();
  auto ampel_city = "";
  int ampel_warnstufe = 0;

  //Check the returning code                                                                  
  if (httpCode > 0) {
    String json = http.getString(); // Get the request response payload
    deserializeJson(doc, json); //, DeserializationOption::Filter(filter)); //Parse message

    ampel_date = doc[0]["Stand"];
    JsonArray Warnstufen = doc[0]["Warnstufen"];
    for(JsonVariant value : Warnstufen) {
      if (value["Name"] == Stadt) {
        ampel_city = value["Name"];
        ampel_warnstufe = value["Warnstufe"].as<int>();
        Serial.println();
        Serial.print("Datenstand: ");
        Serial.println(ampel_date);
        Serial.print("Region: ");
        Serial.println(ampel_city);
      }
    }
  }
  http.end();   //Close connection
  return ampel_warnstufe;
}

void initAmpel() {
  int initTimeout = 800;
  strip.setPixelColor(0, 0, 255, 0); //1. grün
  strip.show();
  strip.show();
  delay(initTimeout);
  clearStrip();

  strip.setPixelColor(1, 255, 255, 0); //2. gelb
  strip.show();
  strip.show();
  delay(initTimeout);
  clearStrip();

  strip.setPixelColor(2, 255, 140, 0); //3. orange
  strip.show();
  strip.show();
  delay(initTimeout);
  clearStrip();

  strip.setPixelColor(3, 255, 0, 0); //4. rot
  strip.show();
  strip.show();
  delay(initTimeout);
  clearStrip();
}

void setAmpelFarbe(int warnstufe) {
  Serial.print ("Warnstufe: ");
  Serial.println (warnstufe);
  Serial.print ("Setze Ampel auf ");
  initAmpel();
  if (warnstufe == 1) {
      Serial.println("grün!");
      strip.setPixelColor(0, 0, 255, 0);
      strip.show();
      strip.show();
  }
  else if (warnstufe == 2) {
      Serial.println("gelb!");
      strip.setPixelColor(1, 255, 255, 0);
      strip.show();
      strip.show();
  }
  else if (warnstufe == 3) {
      Serial.println("orange!");
      strip.setPixelColor(2, 255, 140, 0);
      strip.show();
      strip.show();
  }
  else if (warnstufe == 4) {
      Serial.println("rot!");
      strip.setPixelColor(3, 255, 0, 0);
      strip.show();
      strip.show();
  }
  else if (warnstufe == 0) {
      Serial.println("ungültig!");
      strip.setPixelColor(0, 0, 255, 0);
      strip.setPixelColor(1, 255, 255, 0);
      strip.setPixelColor(2, 255, 140, 0);
      strip.setPixelColor(3, 255, 0, 0);
      strip.show();
      strip.show();
  }
  delay(timeout);
  clearStrip();
  clearStrip();
}

void clearStrip() {
  for( int i = 0; i<NUM_PIXELS; i++){
    strip.setPixelColor(i, 0x000000); strip.show(); strip.show();
  }
}

uint32_t dimColor(uint32_t color, uint8_t width) {
   return (((color&0xFF0000)/width)&0xFF0000) + (((color&0x00FF00)/width)&0x00FF00) + (((color&0x0000FF)/width)&0x0000FF);
}

void knightRider(uint16_t cycles, uint16_t speed, uint8_t width, uint32_t color) {
  uint32_t old_val[NUM_PIXELS]; // up to 256 lights!
  // Larson time baby!
  for(int i = 0; i < cycles; i++){
    for (int count = 0; count<NUM_PIXELS; count++) {
      strip.setPixelColor(count, color);
      old_val[count] = color;
      for(int x = count; x>0; x--) {
        old_val[x-1] = dimColor(old_val[x-1], width);
        strip.setPixelColor(x-1, old_val[x-1]); 
      }
      strip.show();
      delay(speed);
    }
    for (int count = NUM_PIXELS-1; count>=0; count--) {
      strip.setPixelColor(count, color);
      old_val[count] = color;
      for(int x = count; x<=NUM_PIXELS ;x++) {
        old_val[x-1] = dimColor(old_val[x-1], width);
        strip.setPixelColor(x+1, old_val[x+1]);
      }
      strip.show();
      delay(speed);
    }
  }
}

void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}

void measure_battery() {
  double batt = analogRead(34);
  batt = (batt * 1.7) / 10000;
  Serial.print("Batteriestand: ");
  Serial.println(batt);
}

void prepareAndGoToDeepSleep(){
  WiFi.disconnect(true); //Disconnecting
  Serial.println("Gehe schlafen");
  Serial.flush();
  delay(1000);
  esp_deep_sleep_start(); // go to sleep
}