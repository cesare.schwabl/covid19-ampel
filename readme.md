# COVID19-Ampel
modified 01 Oct 2020
by Cesare Schwabl

![Covid19-Ampel](ampel.jpg)

## Benötigte Materialien
* ESP32 Dev Board
  * z.b: Olimex ESP32-DevKit-LiPo 
    https://www.olimex.com/Products/IoT/ESP32/ESP32-DevKit-LiPo/open-source-hardware
* 4 WS2812B LED
  * z.B: https://de.aliexpress.com/item/1942924538.html
* Kabel
* eventuell Leiterplatte, Sockelleisten, 470 Ohm Widerstand

## Software
* Arduino IDE
* eventuell Platformio und VSCode
* Libraries
  * ArduinoJson
  * Adafruit_NeoPixel